<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets',function(Blueprint $table){
            $table->id();
            // Question du ticket
            $table->longText('question');
            // Reponse données par le CT
            $table->longText('reponse')->nullable();
            // Status du ticket 0=> non attribuer, 1 => attribuer, 2=>résolue
            $table->integer('status')->default(0);
            // Poste de l'employé
            $table->string('poste');
            // Email de l'employé
            $table->string('email');
            // SI le ticket doit être partager ou non en base de connaissance
            $table->integer('connaissance')->default(0);
            // Résultat de l'enquête de satisfaction
            $table->integer('satisfaction')->nullable();
            // Indicateur si le ticket à été rendu anonyme ou non, 0 => non
            $table->integer('anonyme')->default(0);
            // Ajout des clef étrangère
            $table->foreignId('id_theme')->nullable();
            $table->foreignId('id_commune');
            $table->foreignId('id_technicien')->nullable();
            // Ajout des date  & du softdelete
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
