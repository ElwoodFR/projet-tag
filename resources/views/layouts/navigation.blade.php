<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="flex-shrink-0 flex items-center">
                    <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                        @if(!Auth::guest() && Auth::User()->type_type == 'App\Models\Commune')
                        <a href="{{ route('commune.accueil') }}">
                            <x-application-logo class="block h-10 w-auto fill-current text-gray-600" />
                        </a>
                        @elseif(!Auth::guest() && Auth::User()->type_type == 'App\Models\Technicien')
                        <a href="{{ route('technicien.accueil') }}">
                            <x-application-logo class="block h-10 w-auto fill-current text-gray-600" />
                        </a>
                        @endif
                    </div>
                </div>

                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    @if(!Auth::guest() && Auth::User()->type_type == 'App\Models\Commune')
                    <x-nav-link :href="route('commune.accueil')" :active="request()->routeIs('dashboard')">
                        {{ __('lang.dashboard') }}
                    </x-nav-link>
                    @elseif(!Auth::guest() && Auth::User()->type_type == 'App\Models\Technicien')
                    <x-nav-link :href="route('technicien.accueil')" :active="request()->routeIs('dashboard')">
                        {{ __('lang.dashboard') }}
                    </x-nav-link>
                    <x-nav-link :href="route('commune.adduser')" :active="request()->routeIs('addUser')">
                        {{ __('lang.add.commune') }}
                    </x-nav-link>
                    <x-nav-link :href="route('technicien.basedeconnaissance')"
                        :active="request()->routeIs('basedeconnaissance')">
                        {{ __('lang.knowledgebase') }}
                    </x-nav-link>
                    <x-nav-link :href="route('technicien.stats')"
                        :active="request()->routeIs('stats')">
                        {{ __('lang.statistique') }}
                    </x-nav-link>
                    @endif
                </div>
            </div>

            <!-- Settings Dropdown -->
            <div class="hidden sm:flex sm:items-center sm:ml-6">
            @php $flag = Config::get('languages')[App::getLocale()]['flag-icon']; @endphp
                <li class="block relative" x-data="{showChildren:false}" @click.away="showChildren=false">
                    <a href="#"
                        class="flex items-center h-10 leading-10 px-4 rounded cursor-pointer no-underline hover:no-underline transition-colors duration-100 mx-1"
                        @click.prevent="showChildren=!showChildren">
                        <img src="{{ asset('images/flags/'.$flag ).'.png' }}" class="mr-2" width="25px"/> {{ Config::get('languages')[App::getLocale()]['display'] }}
                        <span class="caret"></span>
                        <span class="ml-2">
                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20">
                                    <path fill-rule="evenodd"
                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                        clip-rule="evenodd" />
                                </svg>
                        </span>
                    </a>
                    <div class="bg-white shadow-md rounded border border-gray-300 text-sm absolute top-auto left-0 min-w-full w-56 z-30 mt-1"
                        x-show="showChildren" x-transition:enter="transition ease duration-300 transform"
                        x-transition:enter-start="opacity-0 translate-y-2"
                        x-transition:enter-end="opacity-100 translate-y-0"
                        x-transition:leave="transition ease duration-300 transform"
                        x-transition:leave-start="opacity-100 translate-y-0"
                        x-transition:leave-end="opacity-0 translate-y-4" style="display: none;">
                        <span
                            class="absolute top-0 left-0 w-3 h-3 bg-white border transform rotate-45 -mt-1 ml-6"></span>
                        <div class="bg-white rounded w-full relative z-10 py-1">
                            <ul class="list-reset">
                            @foreach (Config::get('languages') as $lang => $language)
                                @if ($lang != App::getLocale())
                                    <li class="relative" x-data="{showChildren:false}" @mouseleave="showChildren=false"
                                        @mouseenter="showChildren=true">
                                        <a href="{{ route('lang.switch', $lang) }}"
                                            class="px-4 py-2 flex w-full items-start hover:bg-gray-100 no-underline hover:no-underline transition-colors duration-100 cursor-pointer">
                                            <img src="{{ asset('images/flags/'.$language['flag-icon']).'.png' }}" class="mr-2"
                                            width="25px">{{$language['display']}}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                            </ul>
                        </div>
                    </div>
                </li>
                <x-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        <button
                            class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div>{{ Auth::user()->email }}</div>

                            <div class="ml-1">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20">
                                    <path fill-rule="evenodd"
                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                        clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                        <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                {{ __('lang.logout') }}
                            </x-dropdown-link>
                        </form>
                    </x-slot>
                </x-dropdown>
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <button @click="open = ! open"
                    class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex"
                            stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round"
                            stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            @if(!Auth::guest() && Auth::User()->type_type == 'App\Models\Commune')
            <x-responsive-nav-link :href="route('commune.accueil')" :active="request()->routeIs('dashboard')">
                {{ __('lang.dashboard') }}
            </x-responsive-nav-link>
            @elseif(!Auth::guest() && Auth::User()->type_type == 'App\Models\Technicien')
            <x-responsive-nav-link :href="route('technicien.accueil')" :active="request()->routeIs('dashboard')">
                {{ __('lang.dashboard') }}
            </x-responsive-nav-link>
            @endif
        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="px-4">
                <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>
                <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>
            </div>

            <div class="mt-3 space-y-1">
                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-responsive-nav-link :href="route('logout')" onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('lang.logout') }}
                    </x-responsive-nav-link>
                </form>
            </div>
        </div>
    </div>
</nav>
