@component('mail::message')
{{__('lang.hello')}},

{{__("lang.email.message")}}

{{__('lang.email.message.two')}} :
@component('mail::panel')
    {{__('lang.email')}} : {{$user->email}}

    {{ __('lang.password')}} : {{$passwordClear}}
@endcomponent

{{__('lang.thank')}},<br>
{{ config('app.name') }}
@endcomponent