<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <span class="uppercase ">{{__('lang.technician') }} </span> - {{__('lang.home') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <ul role="list" class="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3">
                @forelse ($tickets as $ticket)
                    <li class="col-span-1 mx-2 bg-white rounded-lg shadow divide-y divide-gray-200">
                        <div class="w-full flex items-center justify-between p-6 space-x-6">
                            <div class="flex-1 truncate">
                                <div class="flex justify-between space-x-3">
                                    <h3 class="text-gray-900 text-sm font-medium truncate">{{ $ticket->email }}</h3>
                                    <span class="flex-shrink-0 justify-end inline-block px-2 py-1 text-green-800 text-xs font-medium bg-green-100 rounded-full">{{ $ticket->status }}</span>
                                </div>
                                <p class="mt-4 text-gray-900 text-sm truncate">{{__('lang.create_at') }} : {{ $ticket->created_at->format('d/m/Y') }}</p>
                                <p class="mt-4 text-gray-500 text-sm truncate">{{ $ticket->question }}</p>
                            </div>
                        </div>
                        <div>
                            <div class="-mt-px flex divide-x divide-gray-200">
                                  <div class="w-0 flex-1 flex">
                                    <a href="/ticket/answer/{{ $ticket->id }}" class="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500">
                                        <span class="ml-3">{{__('lang.response') }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                @empty
                <p>{{__('lang.no.ticket') }}</p>
                @endforelse
            </ul>
            <div class="mt-10">
                <h2 class="text-2xl">{{__('Satisfaction par communes:')}}</h2>
                <div id="chartSatisfaction"></div>
            </div>
            
        </div>
    </div>
</x-app-layout>
{{-- Ajout d'apexchart en CDN --}}
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
{{-- Ajout du script JS de la page --}}
<script src="{{asset('js/technicien/accueil.js')}}" defer></script>
