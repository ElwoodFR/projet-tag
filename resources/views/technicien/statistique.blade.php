<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <span class="uppercase ">{{__('Technicien') }} </span> - {{__('Statistiques') }}
        </h2>
    </x-slot>
    {{-- Partie affichage des données --}}
    <div class="py-12">
        <div class="md:max-w-screen-sm lg:max-w-screen-md xl:max-w-screen-lg  2xl:max-w-screen-xl mx-auto ">
            {{-- Partie formulaire --}}
            <div>
                <div class="flex">
                    <div class="flex-auto">
                        <label class="block" for="debut_periode">{{__("Selection début de période :")}}</label>
                        <input class='border border-blue-500 rounded h-12' type="date" name="debut_periode" id="debut_periode" value="{{now()->subYears()->format('Y-m-d')}}">
                    </div>
                    <div class="flex-auto">
                        <label class="block" for="fin_periode">{{__('Sélection fin de période :')}}</label>
                        <input class='border border-blue-500 rounded h-12' type="date" name="fin_periode" id="fin_periode" value="{{now()->format('Y-m-d')}}">
                    </div>
                    <div class="flex-auto">
                        <label class="block" for="selection_commune">{{__('Sélection par communes :')}}</label>
                        <select name="commune[]" id="commune" multiple class="appearance-none w-full rounded border h-12 max-h-64 border-blue-500 resize-y">
                            <option value="">---------</option>
                            @foreach($communes as $commune)
                                <option value="{{$commune->id}}">{{$commune->ville}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <button id="getData" class="border border-blue-500 
                        hover:border-transparent  
                        bg-transparent 
                        hover:bg-blue-500 
                        text-blue-500 
                        hover:text-white 
                        my-3 
                        p-2 
                        rounded 
                        bg-white
                        "  
                    type="button">{{__('Afficher')}}</button>
                </div>
            </div>
            {{-- Partie charts --}}
            <div>
                {{-- Partie global --}}
                <div class="grid grids-col-4 gap-8">
                    <p class="text-xl font-bold">{{__('Total:')}} <span id="totalGlobal"><span> </p>
                    <p class="text-xl font-bold">{{__('Total par commune :')}}</p>
                    <div id="totalCommune" class="flex space-x-8"></div>
                    <p class="text-xl font-bold">{{__('Total par theme :')}}</p>
                    <div id="totalTheme" class="flex space-x-8"></div>
                    <p class="text-xl font-bold">{{__('Total par technicien :')}}</p>
                    <div id="totalCT" class="flex space-x-8"></div>
                </div>
                {{-- Partie par mois --}}
                <div class="sm:max-w-sm md:max-w-md lg:max-w-lg xl:max-w-xl 2xl:max-w-2xl">
                    <h2 class="text-xl font-bold">{{__('Charts par mois:')}}</h2>
                    <p>{{__('Global:')}}</p>
                    <div id="chartMonthGlobal"></div>
                    <p>{{__("Communes:")}}</p>
                    <div id="chartMonthCity"></div>
                    <p>{{__("Themes:")}}</p>
                    <div id="chartMonthTheme"></div>
                    <p>{{__("Technicien:")}}</p>
                    <div id="chartMonthCT"></div>
                </div>
                {{-- partie par ans --}}
                <div class="sm:max-w-sm md:max-w-md lg:max-w-lg xl:max-w-xl 2xl:max-w-xl">
                    <h2 class="text-xl font-bold">{{__('Charts par année:')}}</h2>
                    <p>{{__("Global:")}}</p>
                    <div id="chartYearsGlobal"></div>
                    <p>{{__("Communes:")}}</p>
                    <div id="chartYearsCity"></div>
                    <p>{{__("Themes:")}}</p>
                    <div id="chartYearsTheme"></div>
                    <p>{{__('Technicien:')}}</p>
                    <div id="chartYearsCT"></div>
                </div>
            </div>


        </div>
        
    </div>
</x-app-layout>
{{-- Ajout d'apexchart en CDN --}}
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="{{asset('js/technicien/stats.js')}}"></script>