<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        <span class="uppercase ">{{__('lang.technician') }} </span> - {{__('lang.knowledgebase') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div class="bg-gray-200 shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">
                <div class="flex items-center justify-end">
                    <div class="flex border-2 border-white rounded">
                        <input type="text" class="px-4 py-2 w-full border-none outline-none" placeholder="Search...">
                        <button class="flex items-center bg-white justify-center px-4">
                            <svg class="w-6 h-6 text-gray-600" fill="currentColor" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 24 24">
                                <path
                                    d="M16.32 14.9l5.39 5.4a1 1 0 0 1-1.42 1.4l-5.38-5.38a8 8 0 1 1 1.41-1.41zM10 16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z" />
                            </svg>
                        </button>
                    </div>
                </div>

                <ul role="list" class="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3 mt-6">
                    @forelse ($tickets as $ticket)
                    <li class="col-span-1 mx-2 bg-white rounded-lg shadow-xl divide-y divide-gray-200">
                        <div class="w-full flex items-center justify-between p-6 space-x-6">
                            <div class="flex-1 truncate">
                                <div class="flex justify-between space-x-3">
                                    <h3 class="text-gray-900 text-sm font-medium truncate">{{ $ticket->commune->ville }}</h3>
                                    <span
                                        class="flex-shrink-0 justify-end inline-block px-2 py-1 text-green-800 text-xs font-medium bg-green-100 rounded-full">{{ $ticket->status }}</span>
                                </div>
                                <p class="mt-4 text-gray-900 text-sm truncate">{{__('lang.create_at') }} : {{ $ticket->created_at->format('d/m/Y') }}</p>
                                <h1 class="mt-4">Question : </h1>
                                <p class="mt-2 text-gray-500 text-sm truncate">{{ $ticket->question }}</p>
                            </div>
                        </div>
                        <div>
                            <div class="-mt-px flex divide-x divide-gray-200">
                                  <div class="w-0 flex-1 flex">
                                    <a href="/ticket/answer/{{ $ticket->id }}" class="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500">
                                        <span class="ml-3">{{__('lang.see.response') }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    @empty
                    <p>{{__('lang.no.ticket') }}</p>
                    @endforelse
                </ul>
           </div>
        </div>
    </div>
</x-app-layout>
{{-- Ajout du script JS de la page --}}
<script src="{{asset('js/technicien/accueil.js')}}" defer></script>
