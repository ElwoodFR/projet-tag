<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{__('Ticket') }}
        </h2>
    </x-slot>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="py-5">


            <div class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">
                <div class="-mx-3 md:flex mb-6">
                    <div class="w-full px-3">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2"
                            for="question">
                            {{ __('lang.question')}}
                        </label>
                        <p
                            class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 mb-3">
                            {{$ticket->question}}</p>
                    </div>
                </div>
                <form class="form-horizontal" role="form" method="POST" action="/ticket/modify/{{ $ticket->id }}">
                    @csrf
                    <div class="-mx-3 md:flex mb-6">
                        <div class="w-full px-3">
                            <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2"
                                for="reponse">
                                {{ __('lang.response')}}
                            </label>
                            <textarea id="reponse" type="text"
                                class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 mb-3"
                                name="reponse">{{ $ticket->reponse ?? null }}</textarea>
                        </div>
                    </div>
                    <div class="-mx-3 md:flex mb-6">
                        <div class="md:w-1/2 px-3">
                            <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2"
                                for="theme">
                                {{ __('lang.theme')}}
                            </label>
                            <textarea rows="1" id="theme" type="text"
                                class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4"
                                name="theme">{{ $ticket->theme->libelle ?? null }}</textarea>
                        </div>
                        <div class="md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2"
                                for="connaissance">
                                {{__('lang.connaissance')}}
                            </label>
                            <div class="relative">
                                <select
                                    class="block appearance-none w-full bg-grey-lighter border border-grey-lighter text-grey-darker py-3 px-4 pr-8 rounded"
                                    id="connaissance" name="connaissance">
                                    <option selected="true" disabled="disabled">{{__('lang.selected')}}</option>
                                    <option value="1"   
                                        @if($ticket->connaissance)
                                            selected
                                        @endif
                                    >
                                    {{__('lang.yes')}}</option>
                                    <option value="0">{{__('lang.no')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class='flex items-center justify-end  md:gap-8 gap-4 pt-5 pb-5'>
                        <button
                            class='w-auto bg-red-400 hover:bg-red-00 rounded-lg shadow-xl font-medium text-white px-4 py-2'>{{ __('lang.cancel')}}</button>
                        <button type="submit"
                            class='w-auto bg-green-400 hover:bg-green-500 rounded-lg shadow-xl font-medium text-white px-4 py-2'>{{__('lang.send')}}</button>
                    </div>
                </form>
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="px-4 py-5 sm:px-6">
                        <h3 class="text-lg leading-6 font-medium text-gray-900">
                        {{__('lang.abstract')}} :
                        </h3>
                        <p class="mt-1 max-w-2xl text-sm text-gray-500">
                            {{__('lang.ticket.info')}}
                        </p>
                    </div>
                    <div class="border-t border-gray-200 px-4 py-5 sm:p-0">
                        <dl class="sm:divide-y sm:divide-gray-200">
                            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500">
                                    {{__('lang.status')}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{ $ticket->statusText()}}
                                </dd>
                            </div>
                            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500">
                                    {{__('lang.create_at')}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{ $ticket->created_at->format('d/m/Y')}}
                                </dd>
                            </div>
                            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500">
                                    {{__('lang.city')}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$ticket->commune->ville}}
                                </dd>
                            </div>
                            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500">
                                    {{__('lang.post')}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$ticket->poste}}
                                </dd>
                            </div>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-app-layout>
