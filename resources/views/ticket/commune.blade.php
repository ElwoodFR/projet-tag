<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{__('Ticket') }}
        </h2>
    </x-slot>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="py-5">
            <h2 class="font-bold text-xl mt-10">{{ __('lang.question')}}</h2>
            <p class="font-bold  border border-black p-3 bg-white w-full">
                {{$ticket->question}}
            </p>
            <h2 class="font-bold text-xl mt-10">{{ __('lang.response')}}</h2>
            <div class="border border-black bg-white p-10">
                {{ $ticket->reponse ?? null }}
            </div>
            <div class="grid grid-cols-2 gap-x-8 mt-10">
                {{-- Div de notation --}}
                <div>
                    <div>
                        <h4 class="text-xl font-semibold">{{__('lang.status')}}</h4>
                        <p>{{ $ticket->statusText()}}</p>
                    </div>
                    <div>
                        <h4 class="text-xl font-semibold">{{__('lang.create_at')}}</h4>
                        <p>{{ $ticket->created_at->format('d/m/Y')}}</p>
                    </div>
                    @if($ticket->status == 2 && $ticket->satisfaction == null)
                        <h4 class="text-xl font-semibold mt-5">{{__('lang.notation')}}</h4>
                        <form action="{{route('ticket.satisfaction')}}" method="POST">
                            @csrf
                            <input type="hidden" name="ticket" value="{{$ticket->id}}">
                            <div class="flex flex-inline text-xl">
                                <span>1</span>
                                <label for="rating1">
                                    <input type="radio" class="peer sr-only" name="rating" id="rating1" value="1">
                                    <span class="hidden peer-checked:block transform scale-125 text-yellow-500 ">★</span>
                                    <span class="peer-checked:hidden">✩</span>
                                </label>

                                <label for="rating2">
                                    <input type="radio" class="peer sr-only" name="rating" id="rating2" value="2">
                                    <span class="hidden peer-checked:block transform scale-125 text-yellow-500 ">★</span>
                                    <span class="peer-checked:hidden">✩</span>
                                </label>

                                <label for="rating3">
                                    <input type="radio" class="peer sr-only" name="rating" id="rating3" value="3">
                                    <span class="hidden peer-checked:block transform scale-125 text-yellow-500 ">★</span>
                                    <span class="peer-checked:hidden">✩</span>
                                </label>

                                <label for="rating4">
                                    <input type="radio" class="peer sr-only" name="rating" id="rating4" value="4">
                                    <span class="hidden peer-checked:block transform scale-125 text-yellow-500 ">★</span>
                                    <span class="peer-checked:hidden">✩</span>
                                </label>

                                <label for="rating5">
                                    <input type="radio" class="peer sr-only" name="rating" id="rating5" value="5">
                                    <span class="hidden peer-checked:block transform scale-125 text-yellow-500 ">★</span>
                                    <span class="peer-checked:hidden">✩</span>
                                </label>
                                <span>5</span>
                            </div>
                            <button class="border border-blue-500 hover:border-transparent  bg-transparent hover:bg-blue-500 text-blue-500 hover:text-white my-3 p-2 rounded bg-white" 
                                    type="submit">
                                    {{__('lang.send')}}
                                </button>
                        </form>
                    @elseif($ticket->satisfaction != null)
                        <h4 class="text-xl font-semibold mt-5">{{__('lang.stastifaction')}}</h4>
                        @for ($i = 0; $i < $ticket->satisfaction; $i++)
                            <span class="text-yellow-500 ">★</span>
                        @endfor
                        @for ($i = 0; $i < 5 - $ticket->satisfaction ; $i++)
                            <span >✩</span>
                        @endfor
                    @endif
                </div>
                {{-- Div description utilisateur --}}
                <div class="grid grid-cols-2 gap-x-2">
                    <div>
                        <h4 class="text-xl font-semibold">{{__('lang.email')}}</h4>
                        <p>{{$ticket->email}}</p>
                    </div>
                    <div>
                        <h4 class="text-xl font-semibold">{{__('lang.post')}}</h4>
                        <p>{{$ticket->poste}}</p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</x-app-layout>