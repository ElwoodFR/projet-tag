<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <span class="uppercase ">{{$commune->ville}}</span> - {{__('lang.home') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
           <div class="grid gap-x-8 grid-cols-2 ">
                <div>
                    <form action="{{route('ticket.store')}}" class='mx-2' method="post">
                        @csrf
                        <input type="hidden" name="id_commune" id="id_commune" value="{{$commune->id}}">
                        <div>
                            <label class="block" for="question">{{__('lang.question')}}</label>
                            <input class="border rounded w-full" type="text" name="question" id="question" value="" required>
                        </div>
                        <div class="grid grid-cols-2 gap-2 mt-3">
                            <div>
                                <label class="block" for="poste">{{__('lang.post')}}</label>
                                <select name="poste" id="poste">
                                    <option value="Maire">{{__('lang.mayor')}}</option>
                                    <option value="Membre du conseil municipal">{{__('lang.city.council.member')}}</option>
                                    <option value="Employé(e) communale">{{__('lang.municipal.employee')}}</option>
                                </select>
                            </div>
                            <div>
                                <label class="block" for="email">{{__('lang.email')}}</label>
                                <input class="border rounded  w-full" type="email" name="email" value="" required>
                            </div>
                        </div>
                        <button class="border border-blue-500 hover:border-transparent  bg-transparent hover:bg-blue-500 text-blue-500 hover:text-white my-3 p-2 rounded bg-white" 
                            type="submit">
                            {{__('lang.register')}}
                        </button>
                    </form>
                </div>
                {{-- Partie droite -> affichage des tickets --}}
                <div class="border grid grid-cols-2 gap-8" id="ticket_area">
                    
                </div>
           </div>
        </div>
    </div>
</x-app-layout>
{{-- Ajout du script JS de la page --}}
<script src="{{asset('js/commune/accueil.js')}}" defer></script>