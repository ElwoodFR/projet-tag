<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <span class="uppercase ">{{__('lang.technician') }} </span> - {{__('lang.add.commune') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 2xl:max-w-screen-xl">
            <div class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">
                <form action="{{route('commune.adduser')}}" method="post">
                    @csrf
                    <div class="-mx-3 md:flex mb-6">
                        <div class="md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2"
                                for="email">
                                {{__('lang.email')}}
                            </label>
                            <input
                                class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-red rounded py-3 px-4 mb-3"
                                name="email" required type="text" placeholder="example@example.com" required>
                        </div>
                        <div class="md:w-1/2 px-3">
                            <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2"
                                for="adresse">
                                {{__('lang.address')}}
                            </label>
                            <input
                                class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4"
                                name="adresse" type="text" placeholder="" required>
                        </div>
                    </div>
                    <div class="-mx-3 md:flex mb-6">
                        <div class="md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2"
                                for="ville">
                                {{__('lang.city')}}
                            </label>
                            <input
                                class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-red rounded py-3 px-4 mb-3"
                                name="ville" type="text" placeholder="City name" required>
                        </div>
                        <div class="md:w-1/2 px-3">
                            <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2"
                                for="code_postal">
                                {{ __('lang.postal')}}
                            </label>
                            <input
                                class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4"
                                name="code_postal" type="text" placeholder="98748" required>
                        </div>
                    </div>
                    <div class='flex items-center justify-end  md:gap-8 gap-4 pt-5 pb-5'>
                        <button
                            class='w-auto bg-red-400 hover:bg-red-00 rounded-lg shadow-xl font-medium text-white px-4 py-2'>{{ __('lang.cancel')}}</button>
                        <button
                            type="submit"
                            class='w-auto bg-green-400 hover:bg-green-500 rounded-lg shadow-xl font-medium text-white px-4 py-2'>{{__('lang.send')}}</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</x-app-layout>
