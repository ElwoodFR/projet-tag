// IIFE
(()=>{
    // Ecrit le propriété id commune dans le js (Plutôt que passer par un input hidden ou un xslot)
    let id_commune = document.getElementById('id_commune').value;
   
    let road = "/tickets/commune/"+id_commune;

    axios.get(road)
    .then((res)=>{
        let data = res.data;
        let domFragment = document.createDocumentFragment();
        // Pour chaque ticket, créé une carte 
        data.forEach(element => {
            // Créé la balise principal
            let divMain = document.createElement('div');
            let classDivMain = ["max-w-md","py-4","px-2","bg-white","rounded-lg",'relative']
            divMain.classList.add(...classDivMain);
            
            // Créé la balise Titre
            let divTextTitre = document.createElement('h2');
            let divTextTitreClass = ["text-gray-800","text-2xl","font-semibold", "break-words",'mx-3'];
            divTextTitre.classList.add(...divTextTitreClass);
            divTextTitre.textContent = element.question;

            let divTextDate = document.createElement('h3');
            let divTextDateClass = ["text-gray-300"];
            divTextDate.classList.add(...divTextDateClass);
            console.log(element);
            divTextDate.textContent = element.date_creation;

            // Créé la balise qui vas contenir le lien 
            let divLink = document.createElement('div');
            let divLinkClass = ["mt-4"];
            divLink.classList.add(...divLinkClass);

            // Créé la balise A
            let linkAnchor = document.createElement('a');
            linkAnchorClass = ["text-xl","font-medium","absolute","text-blue-500",'bottom-0','right-0'];
            linkAnchor.classList.add(...linkAnchorClass);

            // Setup le HREF
            linkAnchor.href= "/ticket/show/" + element.id;
            linkAnchor.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
          </svg>`;

            // Monte l'element dom 
            divLink.appendChild(linkAnchor);
            divMain.appendChild(divTextTitre);
            divMain.appendChild(divTextDate);
            divMain.appendChild(divLink);
            domFragment.appendChild(divMain)
        });
        document.getElementById('ticket_area').appendChild(domFragment);
    })
})();