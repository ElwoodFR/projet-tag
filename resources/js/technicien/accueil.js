
// Affiche les stats de satisfaction par communes
(()=>{
    axios.get('/ticket/satisfaction/commune')
    .then((res)=>{
        let data = res.data;
        let mySerie = [];
        let categorie = [];
        for(let ville in data){
            categorie.push(ville);
            mySerie.push(parseFloat(data[ville]));
        }
        console.log(mySerie);
        var options = {
            series: [{
                data : mySerie
            }],
            chart:{
                height: 350,
                type: 'bar'
            },
            plotOptions: {
                bar: {
                    columnWidth: '45%',
                    distributed: true,
                }
            },
            xaxis:{
                categories: categorie
            }
        }

        var chart = new ApexCharts(document.getElementById("chartSatisfaction"), options);
        
        chart.render();
    })
})()