// Fichier JS permettant de traiter les statistiques côté technicien
(()=>{
    // Def des const 
    
    const button_get = document.getElementById('getData')

    // Fonction affichant la partie année
    let showStatsYear = (data)=>{
        let categorieYear = [];
        let serieYear = [];
        let serieYearCommune = [];
        let serieYearCt = [];
        let serieYearTheme = [];

        serieYearCommune['unknow'] = [];
        for(let commune of data.communes){
            serieYearCommune[commune.ville] = [];
        }
        serieYearCt['unknow'] = [];
        for(let technicien of data.techniciens){
            serieYearCt[technicien.nom] =[];
        }

        serieYearTheme['unknow'] = [];
        for(let theme of data.themes){
            serieYearTheme[theme.libelle] = []
        }

        for(let year in data.byYears){
            categorieYear.push(year);
            serieYear.push(data.byYears[year].total);
            
            let dataYear = data.byYears[year];
            for(let commune in serieYearCommune){
                if(dataYear.commune[commune] != undefined){
                    serieYearCommune[commune].push(dataYear.commune[commune])
                }
                else{
                    serieYearCommune[commune].push(0);
                }
            }

            for(let theme in serieYearTheme){
                if(dataYear.theme[theme] != undefined){
                    serieYearTheme[theme].push(dataYear.theme[theme])
                }
                else{
                    serieYearTheme[theme].push(0);
                }
            }

            for(let ct in serieYearCt){
                if(dataYear.technicien[ct] != undefined){
                    serieYearCt[ct].push(dataYear.technicien[ct])
                }
                else{
                    serieYearCt[ct].push(0);
                }
            }
        }
        
        let serieFinalCommuneYear = [];
        let serieFinalThemeYear = [];
        let serieFinalTechnicienYear = [];
        for(let commune in serieYearCommune){
            let obj = {
                name: commune,
                data: serieYearCommune[commune]
            };
            serieFinalCommuneYear.push(obj);
        }
       
        for(let ct in serieYearCt){
            let obj = {
                name: ct,
                data: serieYearCt[ct]
            }
            serieFinalTechnicienYear.push(obj)
        }

        for(let theme in serieYearTheme){
            let obj = {
                name: theme,
                data: serieYearTheme[theme]
            }
            serieFinalThemeYear.push(obj)
        }

        //Charts Global
        let globalOptionsYear = {
            chart: {
                type: 'line',
            },
            series:[{
                name: 'total',
                data: serieYear
            }],
            xaxis:{
                categories: categorieYear
            }
        }
        let chartGlobalYear = new ApexCharts(document.getElementById('chartYearsGlobal'),globalOptionsYear);
        chartGlobalYear.render();

        // Charts commune 
        let communeOptionsYear={ 
            chart: {
                type: 'bar',
            },
            series: serieFinalCommuneYear,
            xaxis:{
                categories: categorieYear
            }
        }

        let chartCommuneYear = new ApexCharts(document.getElementById('chartYearsCity'),communeOptionsYear);
        chartCommuneYear.render();

        // Charts Theme 
        let themeOptionsYear={ 
            chart: {
                type: 'bar',
            },
            series: serieFinalThemeYear,
            xaxis:{
                categories: categorieYear
            }
        }

        let chartThemeYear = new ApexCharts(document.getElementById('chartYearsTheme'),themeOptionsYear);
        chartThemeYear.render();

        // Charts CT 
        let ctOptionsYear={ 
            chart: {
                type: 'bar',
            },
            series: serieFinalTechnicienYear,
            xaxis:{
                categories: categorieYear
            }
        }

        let chartCtYear = new ApexCharts(document.getElementById('chartYearsCT'),ctOptionsYear);
        chartCtYear.render();

    };

    
    // Fonction affichant la partie mois
    let showStatsMonth = (data)=>{
        // Définition des catégorie 
        let categorieGlobal = [];
        let serieGlobal = [];
        let serieCommune = [];
        let serieTheme = [];
        let serieCT = [];

        //Créé les matrice commune, theme et technicien
        serieCommune['unknow'] = [];
        for(let commune of data.communes){
            serieCommune[commune.ville]= [];
        }

        serieTheme['unknow'] = []
        for(let theme of data.themes){
            serieTheme[theme.libelle] = [];
        }

        serieCT['unknow'] = [];
        for(let technicien of data.techniciens){
            serieCT[technicien.nom] = [];
        }

        for(let date in data.byMonth){
            categorieGlobal.push(date);
            serieGlobal.push(data.byMonth[date].total);
            let dataDate = data.byMonth[date];
            // Ajout serie Commune
            for(let commune in serieCommune){
                if(dataDate.commune[commune] != undefined){
                    serieCommune[commune].push(dataDate.commune[commune]);
                }
                else{
                    serieCommune[commune].push(0);
                }
            }
            // Ajout serie Theme
            for(let theme in serieTheme){
                if(dataDate.theme[theme] != undefined){
                    serieTheme[theme].push(dataDate.theme[theme]);
                }
                else{
                    serieTheme[theme].push(0);
                }
            }
            // Ajout serie CT
            for(let technicien in serieCT){
                if(dataDate.technicien[technicien] != undefined){
                    serieCT[technicien].push(dataDate.technicien[technicien]);
                }
                else{
                    serieCT[technicien].push(0);
                }
            }

        }
        //Convertie Serie commune en objet pour la charts
        let serieFinalCommune = [];
        let serieFinalCt  = [];
        let serieFinalTheme = [];
        for(let commune in serieCommune){
            let obj = {
                name: commune,
                data: serieCommune[commune]
            };
            serieFinalCommune.push(obj);
        }
       
        for(let ct in serieCT){
            let obj = {
                name: ct,
                data: serieCT[ct]
            }
            serieFinalCt.push(obj)
        }

        for(let theme in serieTheme){
            let obj = {
                name: theme,
                data: serieTheme[theme]
            }
            serieFinalTheme.push(obj)
        }
    

        //Charts Global
        let globalOptions = {
            chart: {
                type: 'line',
            },
            series:[{
                name: 'total',
                data: serieGlobal
            }],
            xaxis:{
                categories: categorieGlobal
            }
        }
        let chartGlobal = new ApexCharts(document.getElementById('chartMonthGlobal'),globalOptions);
        chartGlobal.render();

        // Charts commune 
        let communeOptions={ 
            chart: {
                type: 'bar',
            },
            series: serieFinalCommune,
            xaxis:{
                categories: categorieGlobal
            }
        }

        let chartCommune = new ApexCharts(document.getElementById('chartMonthCity'),communeOptions);
        chartCommune.render();


        // Charts theme 
        let themeOptions={
            chart: {
                type: 'bar',
            },
            series: serieFinalTheme,
            xaxis:{
                categories: categorieGlobal
            }
        }
        let chartTheme = new ApexCharts(document.getElementById('chartMonthTheme'),themeOptions);
        chartTheme.render();
        // Charts ct 

        let ctOptions ={
            chart:{
                type: 'bar',
            },
            series: serieFinalCt,
            xaxis:{
                categories: categorieGlobal
            }
        }
        let chartCT = new ApexCharts(document.getElementById('chartMonthCT'),ctOptions);
        chartCT.render();
        
    }
    // Fonction affichant la partie global
    let showStatsGloblal = (data)=>{
        // Définie les zone
        const totalZone = document.getElementById('totalGlobal');
        const communeZone = document.getElementById('totalCommune');
        const themeZone= document.getElementById('totalTheme');
        const ctZone = document.getElementById('totalCT');
        //Effectue un reset
        totalZone.innerHTML = '';
        communeZone.innerHTML = '';
        themeZone.innerHTML = '';
        ctZone.innerHTML = '';

        // Affiche le total
        totalZone.textContent = data.total;
        // Affiche le total par commune
        let divCommune = document.createDocumentFragment();
        for(let commune in data.commune){
            let div = document.createElement('div');
            let divClass = ["flex-auto",'border','border-black','bg-white','text-center','p-5'];
            div.classList.add(...divClass);
            div.innerHTML = `<p>${commune} : ${data.commune[commune]}</p>`
            divCommune.appendChild(div);
        };
        communeZone.appendChild(divCommune);

        // Affiche le total par theme 
        let divTheme = document.createDocumentFragment();
        for(let theme in data.theme){
            let div = document.createElement('div');
            let divClass = ["flex-auto",'border','border-black','bg-white','text-center','p-5'];
            div.classList.add(...divClass);
            div.innerHTML = `<p>${theme} : ${data.theme[theme]}</p>`
            divTheme.appendChild(div);
        }
        themeZone.appendChild(divTheme);

        // Affiche le total par CT
        let divCT = document.createDocumentFragment();
        for(let technicien in data.technicien){
            let div = document.createElement('div');
            let divClass = ["flex-auto",'border','border-black','bg-white','text-center','p-5'];
            div.classList.add(...divClass);
            div.innerHTML = `<p>${technicien} : ${data.technicien[technicien]}</p>`
            divCT.appendChild(div);

        }
        ctZone.appendChild(divCT);

    }

    let loadPart = ()=>{
        let debut_periode = document.getElementById('debut_periode');
        let fin_periode = document.getElementById('fin_periode');
        let communes = [...document.getElementById('commune').querySelectorAll(':checked')].map(options=>{return options.value}).join("_");
        axios.get('/ticket/stats',{
            params: {
                debut_periode   : debut_periode.value,
                fin_periode     : fin_periode.value,
                communes        : communes
            }
        })
        .then((res)=>{
            let data = res.data;
            showStatsGloblal(data.global);
            showStatsMonth(data);
            showStatsYear(data);
        })

    }

    // Listener d'evenement
    button_get.addEventListener('click',loadPart);
})();
