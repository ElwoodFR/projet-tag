<?php
    return [
        'logout' => 'Log Out',
        'dashboard' => 'Dashboard',
        'knowledgebase' => 'Knowledge base',
        'add.commune' => ' Add Commune',
        'create_at' => 'Create At',
        'response' => 'Response',
        'no.ticket' => 'No ticket',
        'technician' => 'Technician',
        'home' => 'Home',
        'hello' => 'Hello',
        'email' => 'Email',
        'password' => 'Password',
        'address' => 'Address',
        'postal' => 'Postal Code',
        'city' => 'City',
        'send' => 'Submit',
        'cancel' => 'Cancel',
        'thank' => 'Thank You',
        'question' => 'Question',
        'post' => 'Poste',
        'register' => 'Register',
        'status' => 'Status',
        'mayor' => 'Mayor',
        'city.council.member' => 'City Council Member',
        'municipal.employee' => 'Municipal employee',
        'email.message' => 'You receive this email after creating an account for your community on our software',
        'notation' => 'Leave a note of satisfaction',
        'stastifaction' => 'Satisfaction score:',
        'statistique' => 'Statistics',
        'connaissance' => 'Add to the knowledge base ?',
        'theme' => 'Theme',
        'yes' => 'Yes',
        'no' => 'No',
        'selected' => 'Select an answer',
        'abstract' => 'Abstract',
        'ticket.info' => 'Ticket information',
        'email.message.two' => 'In order to simplify your procedures, we provide you with an auto-generated account',
        'created_at : ' => 'Created at : ',
        'see.response' => 'See the answer'


    ];
?>