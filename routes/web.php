<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TraductionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CommuneController;
use App\Http\Controllers\TicketController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Traduction

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\TraductionController@switchLang']);

Route::get('/', function () {
    return view('welcome');
});
// Technicien
Route::get('/technicien',[HomeController::class,'accueil'])->middleware(['auth','techaccess'])->name('technicien.accueil');
Route::get('/technicien/basedeconnaissance',[HomeController::class,'basedeconnaissance'])->middleware(['auth','techaccess'])->name('technicien.basedeconnaissance');
Route::get('/technicien/statistique',[HomeController::class,'statistique'])->middleware(['auth','techaccess'])->name('technicien.stats');


// Commune
Route::get('/commune',[CommuneController::class,'accueil'])->middleware(['auth','communeaccess'])->name('commune.accueil');
Route::match(['get','post'],'/commune/add',[CommuneController::class,'addUser'])->middleware(['auth','techaccess'])->name('commune.adduser');

// Ticket
Route::get('/tickets/commune/{idCommune}',[TicketController::class,'getTicketsByCommune'])->middleware(['auth','communeaccess'])->name('commune.tickets');
Route::get('/ticket/show/{idTicket}',[TicketController::class,'show']);
Route::get('/ticket/answer/{idTicket}',[TicketController::class,'answer']);
Route::post('/ticket/create',[TicketController::class,'store'])->middleware(['auth','communeaccess'])->name('ticket.store');
Route::post('/ticket/modify/{idTicket}',[TicketController::class,'modify'])->middleware(['auth','techaccess']);
Route::post('/ticket/satisfaction',[TicketController::class,'satisfaction'])->middleware(['auth','communeaccess'])->name('ticket.satisfaction');
Route::get('/ticket/satisfaction/commune',[TicketController::class,'satisfactionShow'])->middleware(['auth','techaccess']);
Route::get('/ticket/stats',[TicketController::class,'statistiqueShow'])->middleware(['auth','techaccess']);



require __DIR__.'/auth.php';
