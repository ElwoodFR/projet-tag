const show = document.querySelector("button.show");
const hide = document.querySelector("button.hide");
const menu = document.querySelector(".mobile-menu");

show.addEventListener("click", () => {
    menu.classList.replace("hidden", "block");
});

hide.addEventListener("click", () => {
    menu.classList.replace("block", "hidden");
});

function toggleVideo(state) {
    var div = document.getElementById("popupVid");
    var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
    div.style.display = state == 'hide' ? 'none' : '';
    func = state == 'hide' ? 'pauseVideo' : 'playVideo';
    iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
}