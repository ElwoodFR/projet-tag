<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'adresse',
        'code_postal',
        'ville'
    ];
    /**
     * Add polymorph relationship with User on column 'type'
     */
    public function user(){
        return $this->morphOne(User::class,'type');
    }
    /**
     * Add relationships one to many with Ticket Model
     */
    public function ticket(){
        return $this->hasMany(Ticket::class,'id_commune','id');
    }

}
