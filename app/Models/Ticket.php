<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'question',
        'reponse',
        'poste',
        'email',
        'status',
        'connaissance',
        'satisfaction',
        'anonyme',
        'id_commune',
        'id_technicien',
        'id_theme'
    ];
    /**
     * Get the mairie associated with the ticket.
     */
    public function commune()
    {
        return $this->belongsTo(Commune::class,'id_commune','id');
    }

    /**
     * Get the technicien associated with the ticket.
     */
    public function technicien()
    {
        return $this->belongsTo(Technicien::class,'id_technicien','id');
    }

    /**
     * Get the theme associated with the ticket.
     */
    public function theme()
    {
        return $this->belongsTo(Theme::class,'id_theme','id');
    }

    public function statusText(){
        switch($this->status){
            case 0: 
                return __('En attente');
            case 1:
                return __('Attribué');
            case 2: 
                return __('Résolue');
        }
    }
}
