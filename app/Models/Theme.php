<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{

 protected $fillable = ['libelle'];

    public function ticket() {
        return $this->hasMany(Ticket::class,'id_theme','id');
    }

 }
