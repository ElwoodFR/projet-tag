<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Technicien extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'nom',
        'prenom',
        'telephone'
    ];

    public function tickets()
    {
        return $this->hasMany(Ticket::class,'id_tehcnicien','id');
    }

    public function user(){
        return $this->morphOne(User::class,'type');
    }

}
