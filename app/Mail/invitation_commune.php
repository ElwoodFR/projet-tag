<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class invitation_commune extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $passwordClear;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$passwordClear)
    {
        $this->user = $user;
        $this->passwordClear = $passwordClear;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('projet.tag@apformation.com')
        ->markdown('email.invitation_commune');
    }
}
