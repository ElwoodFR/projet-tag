<?php

namespace App\Http\Controllers;
use App\Models\Commune;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

use App\Mail\invitation_commune;



class CommuneController extends Controller
{
    /**
     * Function Accueil 
     * @return View
     */
    public function accueil(){
        // Récupère la commune commune de l'utilisateur connecté 
        $commune = Commune::where('id',Auth::User()->type_id)->firstOrFail();
       
        return view('commune.accueil',compact('commune'));

    }

    /**
     * Function Add User commune
     */
    public function addUser(Request $request){
        if($request->isMethod('get')){
            return view('commune.add_user');
        }
        if($request->isMethod('post')){

            // Créé la commune 
            $commune = Commune::create([
                'adresse' => $request->adresse,
                'code_postal' => $request->code_postal,
                'ville' => $request->ville
            ]);
            //Génère un mot de passe aléation
            $passwordClear = Str::random(18);
            $passwordHash = Hash::make($passwordClear); 
            // Créé l'utilisateur
            $user = new User();
            $user->email = $request->email;
            $user->password = $passwordHash;
            
            // Associe l'utilisateur à la commune
            $user->type()->associate($commune);
            
            $user->save();
            // Envoie le mail à l'utilisateur
            Mail::to($user->email)->send(new invitation_commune($user,$passwordClear));
            return back()->with('success','Invitation envoyée');
        }
    }
}
