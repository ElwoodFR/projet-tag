<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Models\Commune;
use App\Models\Technicien;
use App\Models\Theme;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use function PHPUnit\Framework\isEmpty;

class TicketController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tickets.index', [
            'tickets' => Ticket::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ticket.create');
    }

    public function store(Request $request) {
        $this->validate($request,[
            'question'=>'required',
            'poste'=>'required',
            'email'=>'required',
            'id_commune' => 'required',
        ]);
        Ticket::create($request->all());
        return redirect('/commune')->with('success','Ticket créé avec succès');
    }

    public function modify(Request $request,$id) {
        $this->validate($request,[
            'reponse'=>'required',
            'theme'=>'required',

        ]);
        $theme = Theme::firstOrCreate(['libelle' => $request->theme]);
        if (Ticket::where('id', $id)->update([
            'reponse' => $request->reponse,
            'status' => 2,
            'id_theme' => $theme->id,
            'connaissance' => $request->connaissance ?? 0])) 
        {
            return redirect('/technicien')->with('success','Ticket résolu');
        } else {
            return 'Failed to update Post';
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::where('id',$id)
        ->with(['commune','theme','technicien'])
        ->first();
        return view('ticket.commune', compact('ticket'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id 
     * @return \Illuminate\Http\Response
     */
    public function answer($id)
    {
        $ticket = Ticket::where('id',$id)
        ->with(['commune','theme','technicien'])
        ->first();
        return view('ticket.technicien', compact('ticket'));
    }

    /**
     * Get the tickets created by a commune
     */
    public function getTicketsByCommune($idCommune) {
       
        if(!isset($idCommune)){
            abort(404);
        }
        
        $tickets =  Ticket::where('id_commune',$idCommune)
        ->whereNull('satisfaction')
        ->get();

        $tickets->map(function($ticket){
            $ticket->date_creation = __('lang.created_at : ').$ticket->created_at->format('d/m/Y');
        });

        return response()->json($tickets);
    }
    /**
     * Add satisfaction info into a ticket
     * @param Request 
     * @return View
     */
    public function satisfaction(Request $request){
        $this->validate($request,[
            'rating' => 'required',
            'ticket' => 'required'
        ]);
       
        $ticket = Ticket::where('id',$request->ticket)->firstOrFail();
        $ticket->satisfaction = $request->rating;
        $ticket->save();
        
        return redirect('/commune')->back()->with('Satisfaction enregistré');
    }
    /**
     * Return satisfaction of each city 
     */
    public function satisfactionShow(){
        $tickets= Ticket::with(['commune'])->whereNotNull('satisfaction')->get();

        $tauxSatisfaction = $tickets->groupBy(function($ticket){
            return $ticket->commune->ville;
        });
        
        $satisfactionFinal = $tauxSatisfaction->mapWithKeys(function($ville,$key){
            return [$key =>[
                $ville->sum('satisfaction') / $ville->count() 
            ]];
        });

        return response()->json($satisfactionFinal);
    }

    /**
     * API Like for statistique request
     */
    public function statistiqueShow(Request $request){
        $this->validate($request,[
            'debut_periode' => 'required',
            'fin_periode' => 'required',
            'communes' => "nullable"
        ]);

        // Création de la requête sql
        $tickets = Ticket::whereBetween('created_at',[$request->debut_periode,$request->fin_periode]);
        if($request->communes != null){
            $communes = explode('_',$request->communes);
            $tickets->whereIn('id_commune',$communes);
        }
        
        $tickets = $tickets
        ->with(['technicien','theme','commune'])
        ->get();
        
        $dateDebut = Carbon::createFromFormat('Y-m-d',$request->debut_periode);
        $dateFin = Carbon::createFromFormat('Y-m-d',$request->fin_periode);

        
        
        // Effectuer les différents trie de stats
        $ticketsMonth  = $this->ticketMonth($tickets,$dateDebut,$dateFin);
        $ticketsYears = $this->ticketYear($tickets,$dateDebut,$dateFin);
        $ticketsGlobal = $this->ticketGlobal($tickets);
        
        $theme = Theme::all();
        $technicien = Technicien::all();
        $communes= Commune::all();

        return response()->json([
            'byMonth' => $ticketsMonth,
            'byYears'=> $ticketsYears,
            'global' => $ticketsGlobal,
            'techniciens' => $technicien,
            'communes'=> $communes,
            'themes' => $theme
        ]);
    }

    /**
     * Permet d'obtenir un tableau associatif des statistiques par mois
     */
    private function ticketMonth($tickets,$dateDebut,$dateFin){
        $months = CarbonPeriod::create($dateDebut,'1 month',$dateFin);
        $ticketMonth = [];
        foreach($months as $month){
            $dateMonth = $month->format('m/Y');
            if(!isset($ticketMonth[$dateMonth])){
                $ticketMonth[$dateMonth] = [];
            }
            
            $debutChargement = $tickets->where('created_at','>',$month->startOfMonth())
            ->where("created_at",'<',$month->endOfMonth());

            $ticketCommune =  $debutChargement ->countBy(function($item){
                return $item->commune->ville ?? __('unknow');
            });

            $ticketTheme =  $debutChargement->countBy(function($ticket){
                return $ticket->theme->libelle ?? __('unknow');
            });

            $ticketTechnicien = $debutChargement->countBy(function($ticket){
                return $ticket->technicien->nom ?? __('unknow');
            });

            $ticketMonth[$dateMonth]['total'] = $debutChargement->count();
            $ticketMonth[$dateMonth]['theme'] = $ticketTheme;
            $ticketMonth[$dateMonth]['commune'] = $ticketCommune;
            $ticketMonth[$dateMonth]['technicien']= $ticketTechnicien;
        }
        return $ticketMonth;
    }
    /**
     * Permet d'obtenir un tableau associatif par ans
     */
    private function ticketYear($tickets,$dateDebut,$dateFin){
        $years = CarbonPeriod::create($dateDebut,'1 year',$dateFin);
        $ticketYear = [];
        foreach($years as $year){
            $dateYear = $year->format('Y');
            if(!isset($ticketsYears[$dateYear])){
                $ticketsYears[$dateYear] = [];
            }
            $debutChargement = $tickets->where('created_at','>',$year->day(1)->month(1))
            ->where('created_at','<',$year->month(12)->endOfMonth());

            $ticketCommune =  $debutChargement ->countBy(function($item){
                return $item->commune->ville ?? __('unknow');
            });

            $ticketTheme =  $debutChargement->countBy(function($ticket){
                return $ticket->theme->libelle ?? __('unknow');
            });

            $ticketTechnicien = $debutChargement->countBy(function($ticket){
                return $ticket->technicien->nom ?? __('unknow');
            });

            $ticketsYears[$dateYear]['total'] = $debutChargement->count();
            $ticketsYears[$dateYear]['theme'] = $ticketTheme;
            $ticketsYears[$dateYear]['commune'] = $ticketCommune;
            $ticketsYears[$dateYear]['technicien']= $ticketTechnicien;
        }
        return $ticketsYears;
    }
    /**
     * Permet d'obtenir les statistiques global pour la période séléctionner
     */
    private function ticketGlobal($tickets){
        $ticketGlobal = [];
        $ticketGlobal['total'] = $tickets->count();

        $ticketGlobal['theme'] = $tickets->countBy(function($ticket){
            return $ticket->theme->libelle ?? __('unknow');
        });
        $ticketGlobal['commune'] = $tickets->countBy(function($ticket){
            return $ticket->commune->ville ?? __('unknow');
        });
        $ticketGlobal['technicien']= $tickets->countBy(function($ticket){
            return $ticket->technicien->nom ?? __('unknow');
        });
        return $ticketGlobal;
    }
}
