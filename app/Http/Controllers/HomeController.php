<?php

namespace App\Http\Controllers;
use App\Models\Technicien;
use App\Models\Ticket;
use App\Models\Commune;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function accueil()
    {
        $tickets = Ticket::where('status','<',2)->get();
        return view('technicien.accueil')->with('tickets', $tickets);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function basedeconnaissance()
    {
        $tickets = Ticket::where('connaissance',1)->get();
        return view('technicien.basedeconnaissance')->with('tickets', $tickets);
    }
    
    /**
     * Show statistiques for technicien
     */
    public function statistique(){
        $communes = Commune::orderBy('ville')->get();
        return view('technicien.statistique',compact('communes'));
    }
}
