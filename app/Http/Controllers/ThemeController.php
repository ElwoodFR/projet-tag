<?php

namespace App\Http\Controllers;

use App\Models\Theme;
use App\Http\Controllers\Controller;

class ThemeController extends Controller {

    public function showThemes(){
        return Themes::all();
    }

    public function update(Request $request, Theme $theme)
    {
        $request->validate([
            'libelle' => 'required',
            
        ]);
        $project->update($request->all());

        return redirect()->route('home')
            ->with('success', 'Theme updated successfully');
    }
    public function destroy(Theme $theme)
    {
        $theme->delete();

        return redirect()->route('home')
            ->with('success', 'Theme deleted successfully');
    }

    public function create()
    {
        return view('theme.create');
    }

  
    public function store(Request $request)
    {
        $request->validate([
            'libelle' => 'required',
            
        ]);

        Theme::create($request->all());

        return redirect()->route('home');
    }
}